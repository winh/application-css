#!/usr/bin/env bash

# Downloads application.css from dev.gitlab.org

set -o errexit
set -o nounset

instance_url=https://dev.gitlab.org
css_url=$(
  curl --silent ${instance_url}/users/sign_in |
    sed -n "s|.*href=\"/\\(assets/application-[a-f0-9]*\\.css\\)|${instance_url}/\\1|p" |
    sed 's|".*||'
)

echo "Downloading ${css_url} ..."
curl \
  --output public/application.css \
  --continue-at - \
  $css_url
