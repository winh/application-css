# application-css

Downloads the application.css from GitLab CE every hour via [Pipeline Schedule] and serves it via GitLab Pages.

With https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/23156 this project is no longer necessary because we have https://gitlab-org.gitlab.io/gitlab-ce/application.css now.

[Pipeline Schedule]: https://docs.gitlab.com/ee/user/project/pipelines/schedules.html
